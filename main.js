const container = document.getElementById('container');
const loading = document.querySelector('.loading');
var page = 1;
var language = 'javascript';

getGitHubLanguages();
getGitHubRepositories();

window.addEventListener('scroll', () => {
	
	const { scrollTop, scrollHeight, clientHeight } = document.documentElement;

	if (clientHeight + scrollTop >= scrollHeight - 5) {
		this.page++; 
		loadNextPage();		
	}

});

function loadNextPage() {
  loading.classList.add('show');
  setTimeout(getGitHubRepositories, 1000);
}

//Busca repositorios na api && popula os cards de acordo com a linguagem selecionada
async function getGitHubRepositories() {	

	let apiResponse = await fetch(`https://api.github.com/search/repositories?q=${getSelectedLanguage()}:???&sort=stars&page=${this.page}`);
	let responseData = await apiResponse.json();
	let repositoriesResponse = responseData.items;
	let repositoriesCards = [];

	repositoriesResponse.forEach((repo, index) => {	

		repoCardData = {		
			'name': repo.name,
			'photoUrl': repo.owner.avatar_url,
			'stars': repo.stargazers_count,
			'forks': repo.forks
		};

		repositoriesCards.push(repoCardData);
	}); 	

	addRepoDataToDOM(repositoriesCards);
	
}

function addRepoDataToDOM(data) {

	data.forEach((item) => {	

		const postElement = document.createElement('div');
		postElement.classList.add('repo-card');

		postElement.innerHTML = `
			<h2 class="title">${item.name}</h2>
			<p class="text">Forks: ${item.forks}</p>
			<p class="text">Stars: ${item.stars}</p>
			<div class="user-info">
				<img src="${item.photoUrl}" alt="${data.name}" />
			</div>
		`;

		container.appendChild(postElement);
		loading.classList.remove('show');
	
	}); 

}

async function getGitHubLanguages() {

	const languageSelect = document.getElementById('languages-select');

	languageSelect.addEventListener('change', evt => {
		updateUI(evt.target.value);		
	});

	let apiResponse = await fetch(`https://api.github.com/languages`);
	let responseData = await apiResponse.json();

	//Popula select que contém linguagens && Define JavaScript como default option
	responseData.forEach((language, index) => {	
		language.name == 'JavaScript' ? languageSelect.options[languageSelect.options.length] = new Option(language.name, language.name, true, true) : languageSelect.options[languageSelect.options.length] = new Option(language.name, language.name);
	}); 

}

//atualiza repository cards baseado na linguagem selecionada
function updateUI(language) {
	this.language = language;	
	clearCards();
	getGitHubRepositories();
}

function getSelectedLanguage() {
	return this.language;
}

//reseta o container dos cards
function clearCards() {
	const cardsContainer = document.getElementById('container');
	cardsContainer.innerHTML = '';
}


